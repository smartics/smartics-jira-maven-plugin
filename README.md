# smartics JIRA Maven Plugin

## Overview

This [plugin](https://maven.apache.org/plugins/index.html)
supports the management of product on a [JIRA](https://www.atlassian.com/software/jira)
server.

More information is available on the [homepage of the smartics JIRA Maven Plugin](https://www.smartics.eu/confluence/x/DICsBg).

## Fork me!
Feel free to fork this project to meet your project requirements.

This plugin is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
