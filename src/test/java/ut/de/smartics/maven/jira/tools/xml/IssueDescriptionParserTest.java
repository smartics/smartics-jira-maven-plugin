/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.jira.tools.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import de.smartics.maven.jira.tools.xml.IssueDescriptionParser;
import de.smartics.maven.jira.tools.xml.IssueDescriptionParser.IssueDescriptor;

import org.junit.Test;

import java.io.File;

import test.de.smartics.maven.jira.cp.FileTestUtils;

/**
 * Tests {@link IssueDescriptionParser}.
 */
public class IssueDescriptionParserTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  // --- tests ----------------------------------------------------------------

  @Test
  public void parseIssueFull() {
    final File file =
        FileTestUtils.getFileFromResource("issues/issue-full.xml");
    final IssueDescriptor descriptor = IssueDescriptionParser.parse(file);

    assertEquals("summary", "Test summary", descriptor.getSummary(null));
    assertEquals("description", "Test description",
        descriptor.getDescription(null));
    assertEquals("type", "New Feature", descriptor.getType(null));
    assertEquals("assignee", "Jane Doe", descriptor.getAssignee(null));
    assertEquals("components", "Component A,Component B",
        descriptor.getComponents(null));
    assertEquals("transition", "Done", descriptor.getTransition(null));
    assertEquals("resolution", "Fixed", descriptor.getResolution(null));
    assertEquals("Link type", "Blocks", descriptor.getLinkType(null));
    assertEquals("Link comment", "Test link comment",
        descriptor.getLinkComment(null));
  }

  @Test
  public void parseIssueEmpty() {
    final File file =
        FileTestUtils.getFileFromResource("issues/issue-empty.xml");
    final IssueDescriptor descriptor = IssueDescriptionParser.parse(file);

    assertNull("summary", descriptor.getSummary(null));
    assertNull("description", descriptor.getDescription(null));
    assertNull("type", descriptor.getType(null));
    assertNull("assignee", descriptor.getAssignee(null));
    assertNull("components", descriptor.getComponents(null));
    assertNull("transition", descriptor.getTransition(null));
    assertNull("resolution", descriptor.getResolution(null));
    assertNull("Link type", descriptor.getLinkType(null));
    assertNull("Link comment", descriptor.getLinkComment(null));
  }

  @Test
  public void parseIssueNone() {
    final File file =
        FileTestUtils.getFileFromResource("issues/issue-none.xml");
    final IssueDescriptor descriptor = IssueDescriptionParser.parse(file);

    assertNull("summary", descriptor.getSummary(null));
    assertNull("description", descriptor.getDescription(null));
    assertNull("type", descriptor.getType(null));
    assertNull("assignee", descriptor.getAssignee(null));
    assertNull("components", descriptor.getComponents(null));
    assertNull("transition", descriptor.getTransition(null));
    assertNull("resolution", descriptor.getResolution(null));
    assertNull("Link type", descriptor.getLinkType(null));
    assertNull("Link comment", descriptor.getLinkComment(null));
  }
}
