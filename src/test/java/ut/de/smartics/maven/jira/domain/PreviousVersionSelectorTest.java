/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.jira.domain;

import static org.junit.Assert.assertTrue;

import de.smartics.maven.jira.domain.PreviousVersionSelector;

import com.atlassian.jira.rest.client.api.domain.Version;

import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Tests {@link PreviousVersionSelector}.
 */
public class PreviousVersionSelectorTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private PreviousVersionSelector uut;

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  @Before
  public void setUp() {
    final List<Version> versions = new ArrayList<>();
    versions.add(createVersion("2.0.1"));
    versions.add(createVersion("1.1.0"));
    versions.add(createVersion("1.0.0"));
    versions.add(createVersion("0.3.0"));
    versions.add(createVersion("0.1.2"));
    versions.add(createVersion("0.1.0"));

    uut = new PreviousVersionSelector(versions);
  }

  private static boolean expectedVersion(final URI uri, final String version) {
    return uri.toASCIIString()
        .endsWith(version);
  }

  // --- helper ---------------------------------------------------------------

  private Version createVersion(String name) {
    try {
      return new Version(new URI("https://example.com/" + name), 0L, name, null,
          false, false, null);
    } catch (final Exception e) {
      throw new IllegalStateException("Cannot create version " + name);
    }
  }

  @Test
  public void insertMicro() {
    final URI uri = uut.select(0, 1, 1);
    assertTrue(expectedVersion(uri, "0.1.0"));
  }

  @Test
  public void insertMinor() {
    final URI uri = uut.select(0, 2, 1);
    assertTrue(expectedVersion(uri, "0.1.2"));
  }


  @Test
  public void insertMajor() {
    final URI uri = uut.select(2, 0, 0);
    assertTrue(expectedVersion(uri, "1.1.0"));
  }

  // --- tests ----------------------------------------------------------------

}
