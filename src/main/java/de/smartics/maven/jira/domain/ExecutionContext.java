/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.domain;

/**
 *
 */
public interface ExecutionContext {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  void logInfo(String message);

  void logDebug(String message);

  /**
   * Verbose is logged on INFO level only if the verbose flag is set.
   */
  void logVerbose(String message);

  /**
   * Verbose is logged on INFO level only if the verbose flag is set.
   */
  void logVerbose(Object appSource);

  boolean isDryRun();

  /**
   * @return
   */
  boolean isVerbose();

  /**
   * @return
   */
  boolean isDebug();

  // --- object basics --------------------------------------------------------

}
