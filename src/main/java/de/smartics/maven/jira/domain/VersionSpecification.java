/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.domain;

import com.atlassian.jira.rest.client.api.domain.Version;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.CheckForNull;

/**
 * Parses a version string and provides its information through its API.
 */
public class VersionSpecification {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Keyword keyword;

  private final ArtifactVersion artifactVersion;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private VersionSpecification(final Keyword keyword,
      final ArtifactVersion artifactVersion) {
    this.keyword = keyword;
    this.artifactVersion = artifactVersion;
  }

  // ****************************** Inner Classes *****************************

  public static enum Keyword {
    /**
     * The keyword indicates that the major version should exist.
     */
    MAJOR("MAJOR", "Ensure that the next major N.x.x version exists.") {
      @Override
      public List<NewVersionDescriptor> nextVersions(
          final VersionSpecification mavenProjectVersion,
          final PreviousVersionSelector selector) {
        final List<NewVersionDescriptor> nextVersions = new ArrayList<>();
        final ArtifactVersion artifactVersion =
            mavenProjectVersion.artifactVersion;
        if (artifactVersion != null) {
          final int newMajor = artifactVersion.getMajorVersion() + 1;
          final String newVersionString = newMajor + ".0.0";
          final NewVersionDescriptor newVersion = new NewVersionDescriptor(
              newVersionString, NewVersionDescriptor.VersionType.MAJOR,
              selector.select(newMajor, 0, 0));
          nextVersions.add(newVersion);
        }
        return nextVersions;
      }
    },

    /**
     * The keyword indicates that the minor version should exist.
     */
    MINOR("MINOR", "Ensure that the next minor x.N.x version exists.") {
      @Override
      public List<NewVersionDescriptor> nextVersions(
          final VersionSpecification mavenProjectVersion,
          final PreviousVersionSelector selector) {
        final List<NewVersionDescriptor> nextVersions = new ArrayList<>();
        final ArtifactVersion artifactVersion =
            mavenProjectVersion.artifactVersion;
        if (artifactVersion != null) {
          final int newMinor = artifactVersion.getMinorVersion() + 1;
          final int major = artifactVersion.getMajorVersion();
          final String newVersionString = major + "." + newMinor + ".0";
          final NewVersionDescriptor newVersion = new NewVersionDescriptor(
              newVersionString, NewVersionDescriptor.VersionType.MINOR,
              selector.select(major, newMinor, 0));
          nextVersions.add(newVersion);
        }
        return nextVersions;
      }
    },

    /**
     * The keyword indicates that the micro version should exist.
     */
    MICRO("MICRO", "Ensure that the next micro x.x.N version exists.") {
      @Override
      public List<NewVersionDescriptor> nextVersions(
          final VersionSpecification mavenProjectVersion,
          final PreviousVersionSelector selector) {
        final List<NewVersionDescriptor> nextVersions = new ArrayList<>();
        final ArtifactVersion artifactVersion =
            mavenProjectVersion.artifactVersion;
        if (artifactVersion != null) {
          final int major =
              mavenProjectVersion.artifactVersion.getMajorVersion();
          final int minor =
              mavenProjectVersion.artifactVersion.getMinorVersion();
          final int newMicro =
              mavenProjectVersion.artifactVersion.getIncrementalVersion() + 1;
          final String newVersionString = major + "." + minor + '.' + newMicro;
          final NewVersionDescriptor newVersion = new NewVersionDescriptor(
              newVersionString, NewVersionDescriptor.VersionType.MICRO,
              selector.select(major, minor, newMicro));
          nextVersions.add(newVersion);
        }
        return nextVersions;
      }
    },

    /**
     * The keyword indicates that the major, minor, and micro versions should
     * exist.
     */
    ALL("ALL",
        "Ensure that the next major, minor, and mirco versions N.N.N exist.") {
      @Override
      public List<NewVersionDescriptor> nextVersions(
          final VersionSpecification mavenProjectVersion,
          final PreviousVersionSelector selector) {
        final List<NewVersionDescriptor> nextVersions = new ArrayList<>();
        final ArtifactVersion artifactVersion =
            mavenProjectVersion.artifactVersion;
        if (artifactVersion != null) {
          nextVersions
              .addAll(VERSION.nextVersions(mavenProjectVersion, selector));
          nextVersions
              .addAll(MICRO.nextVersions(mavenProjectVersion, selector));
          nextVersions
              .addAll(MINOR.nextVersions(mavenProjectVersion, selector));
          nextVersions
              .addAll(MAJOR.nextVersions(mavenProjectVersion, selector));
        }
        return nextVersions;
      }
    },

    /**
     * A version is explicitly specified, no keyword is used.
     */
    VERSION("VERSION",
        "The version to ensure to exist is explicitly specified.") {
      @Override
      public List<NewVersionDescriptor> nextVersions(
          final VersionSpecification mavenProjectVersion,
          final PreviousVersionSelector selector) {
        final List<NewVersionDescriptor> nextVersions = new ArrayList<>();
        final ArtifactVersion artifactVersion =
            mavenProjectVersion.artifactVersion;
        if (artifactVersion != null) {
          final String newVersionString = toNonSnapshotString(artifactVersion);
          final NewVersionDescriptor.VersionType type;
          if (artifactVersion.getIncrementalVersion() == 0) {
            if (artifactVersion.getMinorVersion() == 0) {
              type = NewVersionDescriptor.VersionType.MAJOR;
            } else {
              type = NewVersionDescriptor.VersionType.MINOR;
            }
          } else {
            type = NewVersionDescriptor.VersionType.MICRO;
          }

          final NewVersionDescriptor desc =
              new NewVersionDescriptor(newVersionString, type,
                  selector.select(artifactVersion.getMajorVersion(),
                      artifactVersion.getMinorVersion(),
                      artifactVersion.getIncrementalVersion()));
          nextVersions.add(desc);
        }
        return nextVersions;
      }
    };

    private final String id;
    private final String message;

    private Keyword(final String id, final String message) {
      this.id = id;
      this.message = message;
    }

    public static Keyword fromString(final String versionSpecificationString) {
      for (final Keyword current : values()) {
        if (current.id.equals(versionSpecificationString)) {
          return current;
        }
      }

      return VERSION;
    }

    public static String toHelp() {
      final StringBuilder buffer = new StringBuilder(128);

      buffer.append("Version keywords are:");
      for (final Keyword current : values()) {
        final String line =
            String.format("%6s : %s", current.id, current.message);
        buffer.append('\n')
            .append(line);
      }

      return buffer.toString();
    }

    public String getId() {
      return id;
    }

    public String getMessage() {
      return message;
    }

    public abstract List<NewVersionDescriptor> nextVersions(
        VersionSpecification mavenProjectVersion,
        PreviousVersionSelector selector);

    @Override
    public String toString() {
      return id;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static VersionSpecification fromString(
      final String mavenProjectVersion, final String versionSpecificationString)
      throws IllegalArgumentException {
    final Keyword keyword = Keyword.fromString(versionSpecificationString);
    if (keyword == Keyword.VERSION) {
      if ((StringUtils.isBlank(versionSpecificationString)
          || "VERSION".equals(versionSpecificationString))
          && StringUtils.isBlank(mavenProjectVersion)) {
        throw new IllegalArgumentException(
            "No version can be derived from parameters or POM. Please specify"
                + " a target version with -Dsmartics-jira.targetVersion!");
      }
      final ArtifactVersion artifactVersion =
          "VERSION".equals(versionSpecificationString)
              ? new DefaultArtifactVersion(mavenProjectVersion)
              : new DefaultArtifactVersion(versionSpecificationString);
      return new VersionSpecification(keyword, artifactVersion);
    } else {
      final ArtifactVersion artifactVersion =
          StringUtils.isBlank(mavenProjectVersion) ? null
              : new DefaultArtifactVersion(mavenProjectVersion);
      return new VersionSpecification(keyword, artifactVersion);
    }
  }

  // --- get&set --------------------------------------------------------------

  public boolean isKeyword() {
    return keyword != Keyword.VERSION;
  }

  public Keyword getKeyword() {
    return keyword;
  }

  @CheckForNull
  public ArtifactVersion getArtifactVersion() {
    return artifactVersion;
  }

  // --- business -------------------------------------------------------------

  public List<NewVersionDescriptor> createNewVersionsFor(
      final Iterable<Version> jiraProjectVersions) {
    final List<NewVersionDescriptor> nextVersions = keyword.nextVersions(this,
        new PreviousVersionSelector(jiraProjectVersions));
    return calculateExpectedVersions(jiraProjectVersions, nextVersions);
  }

  protected List<NewVersionDescriptor> calculateExpectedVersions(
      final Iterable<Version> jiraProjectVersions,
      final List<NewVersionDescriptor> expectedVersions) {
    for (final Version jiraProjectVersion : jiraProjectVersions) {
      final String name = jiraProjectVersion.getName();
      final NewVersionDescriptor desc =
          new NewVersionDescriptor(name, null, null);
      if (expectedVersions.remove(desc) && expectedVersions.isEmpty()) {
        return expectedVersions;
      }
    }

    return expectedVersions;
  }

  public static String toNonSnapshotString(final ArtifactVersion version) {
    final String versionString = version.toString();
    if (StringUtils.isBlank(version.getQualifier())) {
      return versionString;
    }

    final int index = versionString.lastIndexOf("-SNAPSHOT");
    if (index != -1) {
      final String nonSnapshotVersion = versionString.substring(0, index);
      return nonSnapshotVersion;
    } else {
      return versionString;
    }
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return keyword.toString()
        + (artifactVersion != null ? ": " + artifactVersion : "");
  }
}
