/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.domain;

import java.net.URI;

import javax.annotation.Nullable;

/**
 *
 */
public class NewVersionDescriptor {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final String name;

  private final VersionType versionType;

  private final URI positionUri;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  NewVersionDescriptor(final String name, final VersionType versionType,
      final URI locationUri) {
    this.name = name;
    this.versionType = versionType;
    this.positionUri = locationUri;
  }

  // ****************************** Inner Classes *****************************

  public static enum VersionType {
    MAJOR, MINOR, MICRO;
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getName() {
    return name;
  }

  public VersionType getVersionType() {
    return versionType;
  }

  @Nullable
  public URI getPositionUri() {
    return positionUri;
  }

  // --- business -------------------------------------------------------------

  /**
   * Returns the hash code of the object.
   *
   * @return the hash code.
   */
  @Override
  public int hashCode() {
    return name.hashCode();
  }

  /**
   * Returns <code>true</code> if the given object is semantically equal to the
   * given object, <code>false</code> otherwise.
   *
   * @param object the instance to compare to.
   * @return <code>true</code> if the given object is semantically equal to the
   *         given object, <code>false</code> otherwise.
   */
  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    } else if (object == null || getClass() != object.getClass()) {
      return false;
    }

    final NewVersionDescriptor other = (NewVersionDescriptor) object;

    return (name.equals(other.name));
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return name + " / " + versionType;
  }
}
