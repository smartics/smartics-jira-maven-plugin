/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.domain;

import com.atlassian.jira.rest.client.api.domain.Version;

import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

/**
 * Helper to select a version from a collection of versions based on some
 * criteria.
 */
public final class PreviousVersionSelector {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The versions to select from.
   */
  private final List<Version> versions;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   *
   * @param versions the versions to select from.
   */
  public PreviousVersionSelector(final Iterable<Version> versions) {
    this.versions = new ArrayList<>(64);
    for (final Version version : versions) {
      this.versions.add(version);
    }
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * Selects the smallest version that is not released.
   *
   * @return the smallest, unreleased version or <code>null</code> if no such
   *         version exists.
   */
  public Version selectNextRelease() {
    Version currentlySelected = null;
    ArtifactVersion currentArtifactVersion = null;

    for (final Version version : versions) {
      if (version.isReleased() || version.isArchived()) {
        continue;
      }
      final ArtifactVersion artifactVersion =
          new DefaultArtifactVersion(version.getName());
      if (isSmallerThan(artifactVersion, currentArtifactVersion)) {
        currentlySelected = version;
        currentArtifactVersion = artifactVersion;
      }
    }

    return currentlySelected;
  }

  public Version selectLastRelease() {
    Version currentlySelected = null;
    ArtifactVersion currentArtifactVersion = null;

    for (final Version version : versions) {
      if (!version.isReleased()) {
        continue;
      }
      final ArtifactVersion artifactVersion =
          new DefaultArtifactVersion(version.getName());
      if (isLargerThan(artifactVersion, currentArtifactVersion)) {
        currentlySelected = version;
        currentArtifactVersion = artifactVersion;
      }
    }

    return currentlySelected;
  }

  /**
   * Selects the version that is prior to this version.
   *
   * @param jiraVersion the version reported by JIRA.
   */
  public URI selectAndInsert(final Version jiraVersion) {
    final String versionString = jiraVersion.getName();
    final ArtifactVersion version = new DefaultArtifactVersion(versionString);
    final Version currentlySelected = selectVersion(version.getMajorVersion(),
        version.getMinorVersion(), version.getIncrementalVersion());
    final int index = versions.indexOf(currentlySelected);
    this.versions.add(index, jiraVersion);
    return currentlySelected == null ? null : currentlySelected.getSelf();
  }

  /**
   * Selects the version that is prior to this version.
   *
   * @param major the major part of the version.
   * @param minor the minor part of the version.
   * @param micro the micro part of the version.
   * @return the largest version that is smaller than the specified version or
   *         <code>null</code> if no such version exists.
   */
  @Nullable
  public URI select(final int major, final int minor, final int micro) {
    final Version currentlySelected = selectVersion(major, minor, micro);
    return currentlySelected == null ? null : currentlySelected.getSelf();
  }

  public Version selectVersion(final int major, final int minor,
      final int micro) {
    Version currentlySelected = null;
    ArtifactVersion currentArtifactVersion = null;

    for (final Version version : versions) {
      final ArtifactVersion artifactVersion =
          new DefaultArtifactVersion(version.getName());
      if (isSmallerThan(artifactVersion, major, minor, micro)) {
        if (isLargerThan(artifactVersion, currentArtifactVersion)) {
          currentlySelected = version;
          currentArtifactVersion = artifactVersion;
        }
      }
    }

    return currentlySelected;
  }

  private boolean isLargerThan(final ArtifactVersion thisVersion,
      final ArtifactVersion thatVersion) {
    if (thatVersion == null) {
      return true;
    }

    final int thisMajor = thisVersion.getMajorVersion();
    final int thatMajor = thatVersion.getMajorVersion();
    if (thisMajor > thatMajor) {
      return true;
    } else if (thisMajor == thatMajor) {
      final int thisMinor = thisVersion.getMinorVersion();
      final int thatMinor = thatVersion.getMinorVersion();
      if (thisMinor > thatMinor) {
        return true;
      } else if (thisMinor == thatMinor) {
        final int thisMicro = thisVersion.getIncrementalVersion();
        final int thatMicro = thatVersion.getIncrementalVersion();
        if (thisMicro > thatMicro) {
          return true;
        }
      }
    }

    return false;
  }

  private boolean isSmallerThan(final ArtifactVersion thisVersion,
      final ArtifactVersion thatVersion) {
    if (thatVersion == null) {
      return true;
    }

    return isSmallerThan(thisVersion, thatVersion.getMajorVersion(),
        thatVersion.getMinorVersion(), thatVersion.getIncrementalVersion());
  }

  private boolean isSmallerThan(final ArtifactVersion thisVersion,
      final int thatMajor, final int thatMinor, final int thatMicro) {
    final int thisMajor = thisVersion.getMajorVersion();
    if (thisMajor < thatMajor) {
      return true;
    } else if (thisMajor == thatMajor) {
      final int thisMinor = thisVersion.getMinorVersion();
      if (thisMinor < thatMinor) {
        return true;
      } else if (thisMinor == thatMinor) {
        final int thisMicro = thisVersion.getIncrementalVersion();
        if (thisMicro < thatMicro) {
          return true;
        }
      }
    }

    return false;
  }

  // --- object basics --------------------------------------------------------

}
