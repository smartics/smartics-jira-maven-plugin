/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.tools.xml;

import de.smartics.maven.jira.domain.AppException;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Parses an issue descriptor.
 */
public class IssueDescriptionParser {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Document document;

  private final IssueDescriptor issueDescriptor;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private IssueDescriptionParser(final File issueDescriptorFile)
      throws ParserConfigurationException, SAXException, IOException {
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    // factory.setValidating(true);
    // factory.setIgnoringElementContentWhitespace(true);
    final DocumentBuilder builder = factory.newDocumentBuilder();
//    builder.setErrorHandler(new ErrorHandler() {
//
//      @Override
//      public void warning(final SAXParseException exception)
//          throws SAXException {
//        System.err.println(exception.getMessage());
//      }
//
//      @Override
//      public void error(final SAXParseException exception) throws SAXException {
//        System.err.println(exception.getMessage());
//      }
//
//      @Override
//      public void fatalError(final SAXParseException exception)
//          throws SAXException {
//        System.err.println(exception.getMessage());
//      }
//    });
    this.document = builder.parse(issueDescriptorFile);
    this.issueDescriptor = new IssueDescriptor(issueDescriptorFile);
  }

  // ****************************** Inner Classes *****************************

  public static final class IssueDescriptor {

    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final File file;

    private String summary;

    private String description;

    private String type;

    private String priority;

    private String assignee;

    private String transition;

    private String resolution;

    private String components;

    private String linkType;

    private String linkComment;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private IssueDescriptor(final File file) {
      this.file = file;
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    public File getFile() {
      return file;
    }

    public String getSummary(final String override) {
      if (StringUtils.isNotBlank(override)) {
        return override;
      }
      return summary;
    }

    public String getDescription(final String override) {
      if (StringUtils.isNotBlank(override)) {
        return override;
      }
      return description;
    }

    public String getType(final String override) {
      if (StringUtils.isNotBlank(override)) {
        return override;
      }
      return type;
    }

    public String getPriority(final String override) {
      if (StringUtils.isNotBlank(override)) {
        return override;
      }
      return priority;
    }

    public String getAssignee(final String override) {
      if (StringUtils.isNotBlank(override)) {
        return override;
      }
      return assignee;
    }

    public String getTransition(final String override) {
      if (StringUtils.isNotBlank(override)) {
        return override;
      }
      return transition;
    }

    public String getResolution(final String override) {
      if (StringUtils.isNotBlank(override)) {
        return override;
      }
      return resolution;
    }

    public String getComponents(final String override) {
      if (StringUtils.isNotBlank(override)) {
        return override;
      }
      return components;
    }

    public String getLinkType(final String override) {
      if (StringUtils.isNotBlank(override)) {
        return override;
      }
      return linkType;
    }

    public String getLinkComment(final String override) {
      if (StringUtils.isNotBlank(override)) {
        return override;
      }
      return linkComment;
    }

    // --- business -----------------------------------------------------------

    // --- object basics ------------------------------------------------------
  }


  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  /**
   * Parses the XML descriptor file.
   *
   * @param issueDescriptorFile the file with XML content that contains the data
   *        for the issues to be created.
   * @return the descriptor's issue information.
   * @throws AppException on any problem regarding the service.
   * @throws IllegalArgumentException if the file cannot be read or the XML
   *         cannot be parsed.
   */
  public static IssueDescriptor parse(final File issueDescriptorFile)
      throws AppException, IllegalArgumentException {
    try {
      final IssueDescriptionParser parser =
          new IssueDescriptionParser(issueDescriptorFile);
      final IssueDescriptor issueDescriptor = parser.parse();
      return issueDescriptor;
    } catch (final ParserConfigurationException e) {
      throw new AppException(
          "Cannot create XML parser to parse issue descriptor '"
              + issueDescriptorFile.getAbsolutePath() + "'.");
    } catch (final SAXException | IOException e) {
      throw new IllegalArgumentException(
          "Issue descriptor '" + issueDescriptorFile.getAbsolutePath()
              + "' cannot be parsed: " + e.getMessage(),
          e);
    }
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  private IssueDescriptor parse() {
    issueDescriptor.summary = parseOptinallyOne("summary");
    issueDescriptor.description = parseOptinallyOne("description");
    issueDescriptor.type = parseOptinallyOne("issue", "type");
    issueDescriptor.priority = parseOptinallyOne("priority");
    issueDescriptor.assignee = parseOptinallyOne("assignee");
    issueDescriptor.components = parseList("component");
    issueDescriptor.transition = parseOptinallyOne("transition");
    issueDescriptor.resolution = parseOptinallyOne("resolution");
    issueDescriptor.linkType = parseOptinallyOne("link", "type");
    issueDescriptor.linkComment = parseOptinallyOne("link", "comment");

    return issueDescriptor;
  }

  private String parseOptinallyOne(final String uniqueParentElementName,
      final String elementName) {
    final NodeList parentNodes =
        document.getElementsByTagName(uniqueParentElementName);
    final int count = parentNodes.getLength();
    switch (count) {
      case 0:
        return null;
      case 1:
        final Node parentNode = parentNodes.item(0);
        final Element element = (Element) parentNode;
        final NodeList selectedChildren =
            fetchDirectChildrenWithTageName(element, elementName);
        return parseOptionallyOne(elementName, selectedChildren);
      default:
        throw new IllegalArgumentException("Expected at most one '"
            + elementName + "' within '" + uniqueParentElementName
            + "' in issue descriptor, but found " + count + ".");
    }
  }

  protected NodeList fetchDirectChildrenWithTageName(
      final Element parentElement, final String tagName) {
    final NodeList children = parentElement.getChildNodes();
    final int childCount = children.getLength();
    final List<Node> elements = new ArrayList<>(childCount);
    final NodeList selectedChildren = new NodeList() {
      @Override
      public Node item(final int index) {
        return elements.get(index);
      }

      @Override
      public int getLength() {
        return elements.size();
      }
    };
    for (int i = 0; i < childCount; i++) {
      final Node child = children.item(i);
      if (child.getNodeType() == Node.ELEMENT_NODE) {
        final Element childElement = (Element) child;
        if (tagName.equals(childElement.getTagName())) {
          elements.add(childElement);
        }
      }
    }
    return selectedChildren;
  }

  private String parseList(final String elementName) {
    final NodeList nodes = document.getElementsByTagName(elementName);
    final int count = nodes.getLength();
    if (count == 0) {
      return null;
    }

    final StringBuilder buffer = new StringBuilder(64);
    for (int i = 0; i < count; i++) {
      final Node node = nodes.item(i);
      final String text = node.getTextContent();
      if (StringUtils.isNotBlank(text)) {
        buffer.append(text);
        if (i < count - 1) {
          buffer.append(',');
        }
      }
    }
    if (buffer.length() == 0) {
      return null;
    }
    return buffer.toString();
  }

  private String parseOptinallyOne(final String elementName)
      throws IllegalArgumentException {
    final NodeList nodes = document.getElementsByTagName(elementName);
    return parseOptionallyOne(elementName, nodes);
  }

  protected String parseOptionallyOne(final String elementName,
      final NodeList nodes) {
    final int count = nodes.getLength();
    switch (count) {
      case 1:
        final Node node = nodes.item(0);
        final String text = node.getTextContent();
        if (StringUtils.isNotBlank(text)) {
          return text;
        }
        // else fall through and return null ...
      case 0:
        return null;
      default:
        throw new IllegalArgumentException("Expected at most one '"
            + elementName + "' in issue descriptor, but found " + count + ".");
    }
  }

  // --- object basics --------------------------------------------------------

}
