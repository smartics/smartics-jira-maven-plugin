/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.tools.types;

import org.codehaus.plexus.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Provides utility functions on strings.
 */
public final class StringUtilities {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final Pattern SPLIT_PATTERN = Pattern.compile("\\s*,\\s*");

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private StringUtilities() {};

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static List<String> splitByCommaToList(final String value) {
    if (value == null) {
      return new ArrayList<>(0);
    }
    final ArrayList<String> list = new ArrayList<>(value.length());
    for (final String element : splitByComma(value, true)) {
      list.add(element);
    }
    return list;
  }

  public static String[] splitByComma(final String value, final boolean trim) {
    return splitBy(value, SPLIT_PATTERN, trim);
  }

  public static String[] splitBy(final String value,
      final Pattern splitPattern) {
    return splitBy(value, splitPattern, true);
  }

  public static String[] splitBy(final String value, final Pattern splitPattern,
      final boolean trim) {
    if (StringUtils.isBlank(value)) {
      return new String[0];
    }

    final String[] tokens = splitPattern.split(value);
    if (trim) {
      for (int i = tokens.length - 1; i >= 0; i--) {
        final String normalizedToken = tokens[i].trim();
        tokens[i] = normalizedToken;
      }
    }
    return tokens;
  }

  public static String normalizeText(final String input) {
    if (input == null) {
      return input;
    }

    final String noNewlines = input.replace('\n', ' ');
    final String stripped = strip(noNewlines);
    return stripped;
  }

  /**
   * Replaces all consecutive white spaces by one space and removes white spaces
   * from the start and end of the string.
   *
   * @param string the string to strip.
   * @return the stripped string.
   */
  public static String strip(final String string) {
    return string.trim()
        .replaceAll("\\s+", " ");
  }

  // --- object basics --------------------------------------------------------

}
