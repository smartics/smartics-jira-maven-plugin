/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.tools.types;

import com.atlassian.jira.rest.client.api.domain.input.ComplexIssueInputFieldValue;
import com.atlassian.jira.rest.client.api.domain.input.FieldInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.VersionInput;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

/**
 * Utilities to deal with JIRA input objects.
 */
public final class JiraInputUtilities {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private JiraInputUtilities() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static String toString(final List<IssueInput> issues) {
    final StringBuilder buffer = new StringBuilder(512);
    for (final IssueInput issue : issues) {
      buffer.append("\n  ");
      for (final FieldInput field : issue.getFields()
          .values()) {
        buffer.append("\n    ")
            .append(String.format("%12s : %s", field.getId(), toString(field)));
      }
    }

    return buffer.toString();
  }

  public static String toString(final FieldInput field) {
    final Object valueObject = field.getValue();
    if (valueObject instanceof ComplexIssueInputFieldValue) {
      return toString((ComplexIssueInputFieldValue) valueObject);
    } else if (valueObject instanceof Iterable<?>) {
      return handleCollection(valueObject);
    } else {
      return String.valueOf(valueObject);
    }
  }

  public static String toString(final ComplexIssueInputFieldValue complex) {
    final Map<String, Object> map = complex.getValuesMap();
    final StringBuilder buffer = new StringBuilder(128);
    for (final Map.Entry<String, Object> entry : map.entrySet()) {
      final String key = entry.getKey();
      final Object valueObject = entry.getValue();
      final String value;
      if (valueObject instanceof ComplexIssueInputFieldValue) {
        value = toString((ComplexIssueInputFieldValue) valueObject);
      } else if (valueObject instanceof Iterable<?>) {
        value = handleCollection(valueObject);
      } else {
        value = String.valueOf(valueObject);
      }
      buffer.append(String.format("%12s : %s", key, value))
          .append("\n        ");
    }

    buffer.setLength(buffer.length() - 9);

    return buffer.toString();
  }

  private static String handleCollection(final Object valueObject) {
    final StringBuilder collectionBuilder = new StringBuilder(64);
    for (final Object item : (Iterable<?>) valueObject) {
      String itemString;
      if (item instanceof ComplexIssueInputFieldValue) {
        itemString = toString((ComplexIssueInputFieldValue) item);
      } else {
        itemString = String.valueOf(item);
      }
      if (collectionBuilder.length() > 0) {
        itemString = itemString.trim();
      }
      collectionBuilder.append(itemString)
          .append(", ");
    }
    if (collectionBuilder.length() > 2) {
      collectionBuilder.setLength(collectionBuilder.length() - 2);
    }
    return collectionBuilder.toString();
  }

  public static String toString(final VersionInput version) {
    final StringBuilder buffer = new StringBuilder(64);
    buffer.append("name=")
        .append(version.getName());
    buffer.append(", projectKey=")
        .append(version.getProjectKey());
    buffer.append(", description='")
        .append(version.getDescription());
    final DateTime releaseDate = version.getReleaseDate();
    buffer.append("', releaseDate=")
        .append(releaseDate != null ? releaseDate : "<none>");
    buffer.append(", isArchived=")
        .append(version.isArchived());
    buffer.append(", isReleased=")
        .append(version.isReleased());
    return buffer.toString();
  }

  // --- object basics --------------------------------------------------------

}
