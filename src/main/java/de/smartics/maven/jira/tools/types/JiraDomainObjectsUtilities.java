/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.tools.types;

import com.atlassian.jira.rest.client.api.domain.Version;

import java.util.List;

/**
 * Utilities to deal with domain objects defined by JIRA.
 */
public final class JiraDomainObjectsUtilities {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private JiraDomainObjectsUtilities() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static String toString(final List<Version> versions) {
    final StringBuilder buffer = new StringBuilder(128);
    for (final Version version : versions) {
      buffer.append(version.getName())
          .append(version.isReleased() ? "(released)" : "")
          .append(", ");
    }
    if (buffer.length() > 2) {
      buffer.setLength(buffer.length() - 2);
    }
    return buffer.toString();
  }

  // --- object basics --------------------------------------------------------

}
