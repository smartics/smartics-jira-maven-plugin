/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Display usage information on smartics JIRA Maven Plugin.
 *
 * @since 1.0
 */
@Mojo(name = "usage", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.NONE)
public class UsageMojo extends AbstractMojo {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final StringBuilder buffer = new StringBuilder(512);
    buffer.append("Usage information for the smartics JIRA Maven Plugin")
        .append("\n=== Detailed Descriptions ===")
        .append(
            "\nUse\n   smartics-jira:help \n" + "to list all available goals.")
        .append("\nUse\n   smartics-jira:help -Ddetail -Dgoal=<goal-name>\n"
            + "to fetch more detailed information on a particular goal.");
    buffer.append("\n\n=== General tips ===")
        .append(
            "\n G1. Use Dry Run:\n   If you are unsure what will happen, use -DdryRun. This will connect to the JIRA server, but only for reading, not for writing.")
        .append(
            "\n G2. Parameters are prefixed with 'smartics-jira' (to address this plugin; e.g. -Dsmartics-jira.verbose), some are prefixed with 'jira' (to address JIRA related information; e.g. -Djira.username), and almost all can be used with no prefix (to address this and other plugins; e.g. -Dverbose).")
        .append(
            "\n G3. More chatty?\n   Use -Dsmartics-jira.verbose or -Dverbose to get more on INFO level, use -X to get DEBUG information.");
    buffer.append("\n\n=== Issue Descriptor ===")
        .append("\n<issue>\n" + "  <summary></summary>\n"
            + "  <description></description>\n" + "  <type></type>\n"
            + "  <assignee></assignee>\n" + "  <components>\n"
            + "    <component></component>\n" + "  </components>\n"
            + "  <state>\n" + "    <transition></transition>\n"
            + "    <resolution></resolution>\n" + "  </state>\n" + "  <link>\n"
            + "    <type></type>\n" + "    <comment></comment>\n"
            + "  </link>\n" + "</issue>");
    buffer.append(
        "\n\nFind more information online:\n   https://www.smartics.eu/confluence/x/DICsBg");

    final Log log = getLog();
    log.info(buffer);
  }

  // --- object basics --------------------------------------------------------

}
