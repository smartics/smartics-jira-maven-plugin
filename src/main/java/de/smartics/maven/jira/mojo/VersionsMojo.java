/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.mojo;

import de.smartics.maven.jira.domain.NewVersionDescriptor;
import de.smartics.maven.jira.domain.PreviousVersionSelector;
import de.smartics.maven.jira.domain.VersionSpecification;
import de.smartics.maven.jira.tools.types.JiraDomainObjectsUtilities;
import de.smartics.maven.jira.tools.types.JiraInputUtilities;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.ProjectRestClient;
import com.atlassian.jira.rest.client.api.VersionRestClient;
import com.atlassian.jira.rest.client.api.domain.Project;
import com.atlassian.jira.rest.client.api.domain.Version;
import com.atlassian.jira.rest.client.api.domain.input.VersionInput;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.atlassian.util.concurrent.Promise;

/**
 * Manages versions for products.
 * <p>
 * To ensure that there are versions for the next micro, minor, and major
 * releases, use <tt>-Dsmartics-jira.targetVersion=ALL</tt>.
 * </p>
 *
 * @since 1.0
 */
@Mojo(name = "versions", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.NONE)
public class VersionsMojo extends AbstractJiraMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The list of project keys to create new versions for.
   * <p>
   * Set with '<tt>-Dsmartics-jira.projectKeys</tt>' or
   * '<tt>-DprojectKeys</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.projectKeys", required = true)
  private List<String> jiraProjectKeys;

  /**
   * If a project is available in the context of this goal's execution, the
   * target version defaults to the version of the Maven project.
   * <p>
   * Defaults to '<tt>ALL</tt>'.
   * </p>
   * <p>
   * Set with '<tt>-Dsmartics-jira.targetVersion</tt>' or
   * '<tt>-DtargetVersion</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.targetVersion", defaultValue = "ALL")
  private String targetVersion;

  /**
   * The description to use for major versions that are create on the JIRA
   * server.
   * <p>
   * Defaults to '<tt>Next major release with breaking changes.</tt>'.
   * </p>
   * <p>
   * Set with '<tt>-Dsmartics-jira.descriptionMajorVersion</tt>' or
   * '<tt>-DdescriptionMajorVersion</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.descriptionMajorVersion")
  private String descriptionMajorVersion;

  /**
   * The description to use for minor versions that are create on the JIRA
   * server.
   * <p>
   * Defaults to '<tt>Next minor release with new features and bugfixes.</tt>'.
   * </p>
   * <p>
   * Set with '<tt>-Dsmartics-jira.descriptionMinorVersion</tt>' or
   * '<tt>-DdescriptionMinorVersion</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.descriptionMinorVersion")
  private String descriptionMinorVersion;

  /**
   * The description to use for micro versions that are create on the JIRA
   * server.
   * <p>
   * Defaults to
   * '<tt>Next micro release with bugfixes and improvements of existing features.</tt>'.
   * </p>
   * <p>
   * Set with '<tt>-Dsmartics-jira.descriptionMicroVersion</tt>' or
   * '<tt>-DdescriptionMicroVersion</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.descriptionMicroVersion")
  private String descriptionMicroVersion;

  private List<String> globalJiraProjectKeys;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public List<String> getJiraProjectKeys() {
    if (null == jiraProjectKeys || jiraProjectKeys.isEmpty()) {
      if (globalJiraProjectKeys == null) {
        globalJiraProjectKeys =
            getUserPropertiesManager().getPropertyAsList("projectKeys");
      }
      return globalJiraProjectKeys;
    }
    return jiraProjectKeys;
  }

  public String getTargetVersion() {
    if (StringUtils.isBlank(targetVersion)) {
      final String globalTargetVersion =
          getUserPropertiesManager().getProperty("targetVersion");
      if (StringUtils.isNotBlank(globalTargetVersion)) {
        return globalTargetVersion;
      }
      targetVersion = "ALL";
    }
    return targetVersion;
  }

  public String getDescriptionMajorVersion() {
    if (StringUtils.isBlank(descriptionMajorVersion)) {
      final String desc =
          getUserPropertiesManager().getProperty("descriptionMajorVersion");
      if (StringUtils.isNotBlank(desc)) {
        return desc;
      }
      descriptionMajorVersion = "Next major release with breaking changes.";
    }
    return descriptionMajorVersion;
  }

  public String getDescriptionMinorVersion() {
    if (StringUtils.isBlank(descriptionMinorVersion)) {
      final String desc =
          getUserPropertiesManager().getProperty("descriptionMinorVersion");
      if (StringUtils.isNotBlank(desc)) {
        return desc;
      }
      descriptionMinorVersion =
          "Next minor release with new features and bugfixes.";
    }
    return descriptionMinorVersion;
  }

  public String getDescriptionMicroVersion() {
    if (StringUtils.isBlank(descriptionMicroVersion)) {
      final String desc =
          getUserPropertiesManager().getProperty("descriptionMicroVersion");
      if (StringUtils.isNotBlank(desc)) {
        return desc;
      }
      descriptionMicroVersion =
          "Next micro release with bugfixes and improvements of existing features.";
    }
    return descriptionMicroVersion;
  }


  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final BaseJiraContext context = new BaseJiraContext();
    if (isSkip()) {
      context.logInfo("Skipping creating versions since skip='true'.");
      return;
    }

    final List<String> jiraProjectKeys = getJiraProjectKeys();
    provideJiraMetadata(jiraProjectKeys);

    init();

    final String mavenProjectVersion = getProjectVersion();
    final String targetVersion = getTargetVersion();
    final VersionSpecification targetVersionSpecification =
        VersionSpecification.fromString(mavenProjectVersion, targetVersion);

    context.logVerbose(
        "Target version is defined to '" + targetVersionSpecification + "'.");

    boolean hasFailures = false;
    try (final JiraRestClient client = createJiraClient(context)) {
      final ProjectRestClient projectClient = client.getProjectClient();
      final VersionRestClient versionClient = client.getVersionRestClient();

      for (final String jiraProjectKey : jiraProjectKeys) {
        context.logVerbose(
            "Checking JIRA project '" + jiraProjectKey + "' for versions ...");
        final Promise<Project> projectPromise =
            projectClient.getProject(jiraProjectKey);
        try {
          final Project jiraProject = projectPromise.get();

          final Iterable<Version> jiraProjectVersions =
              jiraProject.getVersions();

          final PreviousVersionSelector previousVersionSelector =
              new PreviousVersionSelector(jiraProjectVersions);
          final List<Version> versions = new ArrayList<>();
          for (final Version version : jiraProjectVersions) {
            versions.add(version);
          }
          if (context.isVerbose()) {
            context.logVerbose(
                "  Existing versions on JIRA project '" + jiraProjectKey + "': "
                    + JiraDomainObjectsUtilities.toString(versions));
          }

          final List<NewVersionDescriptor> newVersions =
              targetVersionSpecification.createNewVersionsFor(versions);
          if (newVersions.isEmpty()) {
            context
                .logVerbose("  -> No new versions required for JIRA project '"
                    + jiraProjectKey + "'.");
          }
          for (final NewVersionDescriptor newVersion : newVersions) {
            final String description = calcDescription(newVersion);
            final VersionInput version = new VersionInput(jiraProjectKey,
                newVersion.getName(), description, null, false, false);
            if (!context.isDryRun()) {
              Promise<Version> versionPromise =
                  versionClient.createVersion(version);
              final Version createdVersion = versionPromise.get();
              final URI afterVersionUri =
                  previousVersionSelector.selectAndInsert(createdVersion);
              if (afterVersionUri != null) {
                versionPromise = versionClient.moveVersionAfter(
                    createdVersion.getSelf(), afterVersionUri);
                final Version movedVersion = versionPromise.get();
                context.logDebug("Moved version '" + movedVersion.getName()
                    + "' after version '" + afterVersionUri + "'.");
              } else {
                context.logWarn(
                    "Cannot move version to correct position since position URI is missing in response.");
              }
              context.logVerbose(
                  "  Version '" + version.getName() + "' on JIRA project '"
                      + jiraProjectKey + "' successfully checked.");
            } else {
              context.logInfo("If not in dryRun mode: Create version: "
                  + JiraInputUtilities.toString(version));
            }
          }
        } catch (final InterruptedException e) {
          hasFailures = true;
          context.logError("Skipping '" + jiraProjectKey
              + "' because timed out talking to JIRA: " + e.getMessage());
        } catch (final ExecutionException e) {
          hasFailures = true;
          logRestCommunicationProblems(context, jiraProjectKey, e);
        }
      }
    } catch (final IOException e) {
      throw new MojoExecutionException(
          "Cannot create REST client to connect to JIRA: " + e.getMessage(), e);
    }
    if (hasFailures) {
      throw new MojoExecutionException(
          "Could not update version information for all projects"
              + " successfully. Check the error log for details.");
    }
  }

  private String calcDescription(final NewVersionDescriptor newVersion) {
    switch (newVersion.getVersionType()) {
      case MAJOR:
        return getDescriptionMajorVersion();
      case MINOR:
        return getDescriptionMinorVersion();
      case MICRO:
        return getDescriptionMicroVersion();
      default:
        return "Next release.";
    }
  }

  // --- object basics --------------------------------------------------------

}
