/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.mojo;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.ProjectRestClient;
import com.atlassian.jira.rest.client.api.RestClientException;
import com.atlassian.jira.rest.client.api.domain.Project;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.atlassian.util.concurrent.Promise;

/**
 * Currently this goal only displays the differences between metadata in the POM
 * and in the referenced JIRA project.
 * <p>
 * In future releases this goal will sync information for a project on the JIRA
 * server with the POM information. Updates will only be applied to the project
 * on JIRA.
 * </p>
 *
 * @since 1.0
 */
@Mojo(name = "sync", threadSafe = true, requiresProject = true,
    defaultPhase = LifecyclePhase.NONE)
public class ProjectSyncMojo extends AbstractJiraMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The key that identifies the project on the JIRA server.
   * <p>
   * Set with '<tt>-Dsmartics-jira.projectKey</tt>' or '<tt>-DprojectKey</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.projectKey")
  private String jiraProjectKey;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getJiraProjectKey() {
    if (StringUtils.isBlank(jiraProjectKey)) {
      return getUserPropertiesManager().getProperty("projectKey");
    }
    return jiraProjectKey;
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final BaseJiraContext context = new BaseJiraContext();
    if (isSkip()) {
      context.logInfo("Skipping project sync since skip='true'.");
      return;
    }

    init();

    try (final JiraRestClient client = createJiraClient(context)) {
      final ProjectRestClient projectClient = client.getProjectClient();

      final String jiraProjectKey = getJiraProjectKey();
      final Promise<Project> projectPromise =
          projectClient.getProject(jiraProjectKey);
      try {
        final Project jiraProject = projectPromise.get();
        updateJiraProject(context, projectClient, jiraProject);
      } catch (final InterruptedException e) {
        context.logError("Skipping '" + jiraProjectKey
            + "' because timed out talking to JIRA: " + e.getMessage());
      } catch (final ExecutionException e) {
        final Throwable cause = e.getCause();
        if (cause instanceof RestClientException) {
          final RestClientException restCause = (RestClientException) cause;
          final int statusCode = restCause.getStatusCode()
              .get();
          if (statusCode == 404) {
            createJiraProject(context, projectClient);
            return;
          }
        }
        logRestCommunicationProblems(context, jiraProjectKey, e);
      }
    } catch (final IOException e) {
      throw new MojoExecutionException(
          "Cannot create REST client to connect to JIRA: " + e.getMessage(), e);
    }
  }

  private void createJiraProject(final BaseJiraContext context,
      final ProjectRestClient projectClient) {
    context.logInfo("Project does not yet exist on JIRA.");
  }

  private void updateJiraProject(final BaseJiraContext context,
      final ProjectRestClient projectClient, final Project jiraProject) {
    final String name = mavenProject.getName();
    if (StringUtils.isNotBlank(name)) {
      final String jiraName = jiraProject.getName();
      if (!name.equals(jiraName)) {
        context.logInfo("Project name differs:\n  JIRA : " + jiraName
            + "\n  Maven: " + name);
      }
    }

    final String description = mavenProject.getDescription();
    if (StringUtils.isNotBlank(description)) {
      final String jiraDesciption = jiraProject.getDescription();
      if (!description.equals(jiraDesciption)) {
        context.logInfo("Project description differs:\n  JIRA : "
            + jiraDesciption + "\n  Maven: " + description);
      }
    }

    final String url = mavenProject.getUrl();
    if (StringUtils.isNotBlank(url)) {
      final URI jiraUrl = jiraProject.getUri();
      final String jiraUrlString =
          jiraUrl != null ? jiraUrl.toASCIIString() : null;
      if (!url.equals(jiraUrlString)) {
        context.logInfo("Project URL differs:\n  JIRA : " + jiraUrlString
            + "\n  Maven: " + url);
      }
    }
  }

  @Override
  protected void init() {
    final String jiraProjectKey = getJiraProjectKey();
    final List<String> jiraProjectKeys = new ArrayList<>(1);
    if (StringUtils.isNotBlank(jiraProjectKey)) {
      jiraProjectKeys.add(jiraProjectKey);
      provideJiraMetadata(jiraProjectKeys);
    } else {
      provideJiraMetadata(jiraProjectKeys);
      if (!jiraProjectKeys.isEmpty()) {
        this.jiraProjectKey = jiraProjectKeys.get(0);
      }
    }

    super.init();
  }

  // --- object basics --------------------------------------------------------

}
