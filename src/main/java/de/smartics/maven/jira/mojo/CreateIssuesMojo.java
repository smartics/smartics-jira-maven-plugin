/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.mojo;

import de.smartics.maven.jira.domain.PreviousVersionSelector;
import de.smartics.maven.jira.tools.types.JiraInputUtilities;
import de.smartics.maven.jira.tools.types.StringUtilities;
import de.smartics.maven.jira.tools.xml.IssueDescriptionParser;
import de.smartics.maven.jira.tools.xml.IssueDescriptionParser.IssueDescriptor;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.MetadataRestClient;
import com.atlassian.jira.rest.client.api.ProjectRestClient;
import com.atlassian.jira.rest.client.api.RestClientException;
import com.atlassian.jira.rest.client.api.domain.BasicIssue;
import com.atlassian.jira.rest.client.api.domain.BasicPriority;
import com.atlassian.jira.rest.client.api.domain.BulkOperationErrorResult;
import com.atlassian.jira.rest.client.api.domain.BulkOperationResult;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.IssueType;
import com.atlassian.jira.rest.client.api.domain.Priority;
import com.atlassian.jira.rest.client.api.domain.Project;
import com.atlassian.jira.rest.client.api.domain.Resolution;
import com.atlassian.jira.rest.client.api.domain.Transition;
import com.atlassian.jira.rest.client.api.domain.Version;
import com.atlassian.jira.rest.client.api.domain.input.ComplexIssueInputFieldValue;
import com.atlassian.jira.rest.client.api.domain.input.FieldInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.api.domain.input.LinkIssuesInput;
import com.atlassian.jira.rest.client.api.domain.input.TransitionInput;
import com.atlassian.jira.rest.client.api.domain.util.ErrorCollection;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import io.atlassian.util.concurrent.Promise;

/**
 * Creates issues on projects. Allows to create issues in projects that take
 * advantage of a fix in another project.
 * <p>
 * In case you intend to go with defaults, run
 * <tt>mvn smartics-jira:issues -DprojectKeys=MYSUBPROJ1,MYSUBPROJ2 -DrootIssue=MYPROJ-7 -DdryRun</tt>
 * for a dry run.
 * </p>
 *
 * @since 1.0
 */
@Mojo(name = "issues", threadSafe = true, requiresProject = false,
    defaultPhase = LifecyclePhase.NONE)
public class CreateIssuesMojo extends AbstractJiraMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String FIELD_KEY_RESOLUTION = "resolution";

  // --- members --------------------------------------------------------------

  /**
   * The list of project keys to create new issues for.
   * <p>
   * The value is <strong>required</strong>.
   * <p>
   * <p>
   * Set with '<tt>-Dsmartics-jira.projectKeys</tt>' or
   * '<tt>-DprojectKeys</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.projectKeys")
  private List<String> jiraProjectKeys;

  /**
   * The issue is the change that influences features in other projects.
   * <p>
   * The value is <strong>required</strong>.
   * <p>
   * Set with '<tt>-Dsmartics-jira.rootIssue</tt>' or '<tt>-DrootIssue</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.rootIssue")
  private String rootIssue;

  /**
   * The summary to set to the related issues. Defaults to the summary of the
   * root issue.
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueSummary</tt>' or
   * '<tt>-DissueSummary</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueSummary")
  private String issueSummary;

  /**
   * The description to set to the related issues.
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueDescription</tt>' or
   * '<tt>-DissueDescription</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueDescription")
  private String issueDescription;

  /**
   * Allows to provide no description with
   * <tt>-Dsmartics-jira.issueDescription</tt>. Per default such a description
   * is required. If this flag is set to <code>true</code>, then there will be
   * no warning and the process will continue if no description is provided.
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueDescriptionIsOptional</tt>' or
   * '<tt>-DissueDescriptionIsOptional</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueDescriptionIsOptional",
      defaultValue = "false")
  private boolean issueDescriptionIsOptional;

  /**
   * The type to set to the related issues. Defaults to the type of the root
   * issue.
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueType</tt>' or '<tt>-DissueType</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueType")
  private String issueType;

  /**
   * The priority to set to the related issues. Defaults to the type of the root
   * issue.
   * <p>
   * Set with '<tt>-Dsmartics-jira.issuePriority</tt>' or
   * '<tt>-DissuePriority</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issuePriority")
  private String issuePriority;

  /**
   * The transition to make for the new issue. If not set, no transition is
   * made.
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueTransition</tt>' or
   * '<tt>-DissueTransition</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueTransition")
  private String issueTransition;

  /**
   * The resolution to set to the related issues. Defaults to the resolution of
   * the root issue. Only taken into account, if a transition is specified.
   * <p>
   * If the resolution should not be set, set this value to
   * <tt>smartics-jira.skip</tt>.
   * </p>
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueResolution</tt>' or
   * '<tt>-DissueResolution</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueResolution")
  private String issueResolution;

  /**
   * The assignee to set to the related issues. Defaults to the assignee of the
   * root issue.
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueAssignee</tt>' or
   * '<tt>-DissueAssignee</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueAssignee")
  private String issueAssignee;

  /**
   * The components to set to the related issues.
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueComponents</tt>' or
   * '<tt>-DissueComponents</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueComponents")
  private String issueComponents;

  /**
   * The type of the link to create from the root issue to each of the created
   * issues. If no link type is provided, no links will be generated.
   * <p>
   * Defaults to <tt>Blocks</tt>.
   * </p>
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueLinkType</tt>' or
   * '<tt>-DissueLinkType</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueLinkType")
  private String issueLinkType;

  /**
   * The comment to the link from the root issue to each of the created issues.
   * If no link comment is provided, no comment will be generated. If no link
   * type is specified, the comment will not be used.
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueLinkComment</tt>' or
   * '<tt>-DissueLinkComment</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueLinkComment")
  private String issueLinkComment;

  /**
   * A reference to a file with information for the issue to be created. If the
   * reference points to a folder, the mojo derives the name of the file from
   * the name of the root issue (adding the file extension <tt>xml</tt>).
   * <p>
   * Set with '<tt>-Dsmartics-jira.issueDescriptorFile</tt>' or
   * '<tt>-DissueDescriptorFile</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.issueDescriptorFile")
  private File issueDescriptorFile;

  private List<String> globalProjectKeys;
  private File globalIssueDescriptorFile;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public List<String> getJiraProjectKeys() {
    if (null == jiraProjectKeys || jiraProjectKeys.isEmpty()) {
      if (null == globalProjectKeys) {
        globalProjectKeys =
            getUserPropertiesManager().getPropertyAsList("projectKeys");
      }
      return globalProjectKeys;
    }
    return jiraProjectKeys;
  }


  public String getRootIssue() {
    if (StringUtils.isBlank(rootIssue)) {
      return getUserPropertiesManager().getProperty("rootIssue");
    }
    return rootIssue;
  }


  public String getIssueSummary() {
    if (StringUtils.isBlank(issueSummary)) {
      return getUserPropertiesManager().getProperty("issueSummary");
    }
    return issueSummary;
  }


  public String getIssueDescription() {
    if (StringUtils.isBlank(issueDescription)) {
      return getUserPropertiesManager().getProperty("issueDescription");
    }
    return issueDescription;
  }

  public boolean isIssueDescriptionIsOptional() {
    return issueDescriptionIsOptional || getUserPropertiesManager()
        .getPropertyAsBoolean("issueDescriptionIsOptional");
  }

  public String getIssueType() {
    if (StringUtils.isBlank(issueType)) {
      return getUserPropertiesManager().getProperty("issueType");
    }
    return issueType;
  }

  public String getIssuePriority() {
    if (StringUtils.isBlank(issuePriority)) {
      return getUserPropertiesManager().getProperty("issuePriority");
    }
    return issuePriority;
  }

  public String getIssueTransition() {
    if (StringUtils.isBlank(issueTransition)) {
      return getUserPropertiesManager().getProperty("issueTransition");
    }
    return issueTransition;
  }


  public String getIssueResolution() {
    if (StringUtils.isBlank(issueResolution)) {
      return getUserPropertiesManager().getProperty("issueResolution");
    }
    return issueResolution;
  }


  public String getIssueAssignee() {
    if (StringUtils.isBlank(issueAssignee)) {
      return getUserPropertiesManager().getProperty("issueAssignee");
    }
    return issueAssignee;
  }


  public String getIssueComponents() {
    if (StringUtils.isBlank(issueComponents)) {
      return getUserPropertiesManager().getProperty("issueComponents");
    }
    return issueComponents;
  }


  public String getIssueLinkType() {
    if (StringUtils.isBlank(issueLinkType)) {
      return getUserPropertiesManager().getProperty("issueLinkType");
    }
    return issueLinkType;
  }


  public String getIssueLinkComment() {
    if (StringUtils.isBlank(issueLinkComment)) {
      return getUserPropertiesManager().getProperty("issueLinkComment");
    }
    return issueLinkComment;
  }


  public File getIssueDescriptorFile() {
    File file;
    if (issueDescriptorFile == null) {
      if (globalIssueDescriptorFile == null) {
        globalIssueDescriptorFile =
            getUserPropertiesManager().getPropertyAsFile(
                mavenProject.getBasedir(), "issueDescriptorFile");
      }
      file = globalIssueDescriptorFile;
    } else {
      file = issueDescriptorFile;
    }

    if (file != null && file.isDirectory()) {
      final String fileName = getRootIssue() + ".xml";
      file = new File(file, fileName);
    }

    if (file == null) {
      final String fileName = getRootIssue() + ".xml";
      final File baseDir = mavenProject != null ? mavenProject.getBasedir()
          : new File(System.getProperty("user.dir"));
      final File testFile = new File(baseDir, fileName);
      if (testFile.canRead()) {
        file = testFile;
      }
    }

    return file;
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    final BaseJiraContext context = new BaseJiraContext();

    if (isSkip()) {
      context.logInfo("Skipping creating issues since skip='true'.");
      return;
    }

    init(context);

    try (final JiraRestClient client = createJiraClient(context)) {
      final IssueRestClient issueClient = client.getIssueClient();

      final String rootIssue = getRootIssue();
      final Promise<Issue> issuePromise = issueClient.getIssue(rootIssue);
      final List<String> jiraProjectKeys = getJiraProjectKeys();
      try {
        final Issue jiraRootIssue = issuePromise.get();
        final List<IssueInput> issues = new ArrayList<>(jiraProjectKeys.size());

        context.logVerbose("Creating issue request ...");
        for (final String jiraProjectKey : jiraProjectKeys) {
          context
              .logVerbose("  Processing project '" + jiraProjectKey + "' ...");
          final IssueInput input =
              createIssue(context, client, jiraProjectKey, jiraRootIssue);
          if (input != null) {
            issues.add(input);
          } else {
            context.logVerbose("    Project '" + jiraProjectKey
                + "' does not provide issue information. No related issue will be created for this project.");
          }
        }

        if (!context.isDryRun()) {
          context.logVerbose("Launching issue request ...");
          if (context.isDebug()) {
            context
                .logDebug("  Request: " + JiraInputUtilities.toString(issues));
          }
          final Promise<BulkOperationResult<BasicIssue>> issuesCreationPromise =
              issueClient.createIssues(issues);
          final BulkOperationResult<BasicIssue> result =
              issuesCreationPromise.get();

          context.logInfo("Processing issue response ...");
          boolean hasFailures = handleErrors(context, result);
          if (!hasFailures) {
            context.logInfo("  Processing issues ...");
          }
          for (final BasicIssue issue : result.getIssues()) {
            context.logVerbose(
                "    Processing issue '" + issue.getKey() + "' ...");
            try {
              context.logVerbose("      Linking '" + issue.getKey() + "' to '"
                  + rootIssue + "' ...");
              linkIssue(issueClient, jiraRootIssue, issue);
            } catch (final InterruptedException e) {
              context.logError("Skipping linking issue '" + issue.getKey()
                  + "' to '" + rootIssue
                  + "' because timed out talking to JIRA: " + e.getMessage());
            } catch (final ExecutionException e) {
              hasFailures = true;
              final String message = "Skipping linking issue '" + issue.getKey()
                  + "' to '" + rootIssue + "': ";
              logRestCommunicationProblems(context, rootIssue, message, e);
            }
            try {
              context.logVerbose("      Resolving '" + issue.getKey() + "' to '"
                  + rootIssue + "' ...");
              resolveIssue(issueClient, jiraRootIssue, issue);
            } catch (final InterruptedException e) {
              context.logError("Skipping resolving issue '" + issue.getKey()
                  + "' related to '" + rootIssue
                  + "' because timed out talking to JIRA: " + e.getMessage());
            } catch (final ExecutionException e) {
              hasFailures = true;

              final String message = "Skipping resolving issue '"
                  + issue.getKey() + "' related to '" + rootIssue + "': ";
              logRestCommunicationProblems(context, rootIssue, message, e);
            }
          }

          if (hasFailures) {
            throw new MojoExecutionException("Could not create all issues "
                + " successfully. Check the error log for details.");
          }
        } else {
          final String issueLinkType = getIssueLinkType();
          final String issueLinkComment = getIssueLinkComment();
          context
              .logInfo("If not in dryRun mode: Create issues:"
                  + JiraInputUtilities.toString(issues)
                  + (StringUtils.isNotBlank(issueLinkType)
                      ? "\n  Links to the " + rootIssue + " will be generated"
                          + (StringUtils.isNotBlank(issueLinkComment)
                              ? " with comment '" + issueLinkComment + "'."
                              : ".")
                      : ""));
        }
      } catch (final InterruptedException e) {
        context.logError("Skipping issue '" + rootIssue
            + "' because timed out talking to JIRA: " + e.getMessage());
      } catch (final ExecutionException e) {
        final Throwable cause = e.getCause();
        if (cause instanceof RestClientException) {
          final RestClientException restCause = (RestClientException) cause;
          final int statusCode = restCause.getStatusCode().get();
          if (statusCode == 404) {
            throw new MojoFailureException("Cannot find issue '" + rootIssue
                + "'. Skipping generation of related issues.");
          }
        }
        logRestCommunicationProblems(context, rootIssue, e);
      }
    } catch (final IOException e) {
      throw new MojoExecutionException(
          "Cannot create REST client to connect to JIRA: " + e.getMessage(), e);
    } catch (final IllegalArgumentException e) {
      throw new MojoFailureException("Cannot create issues: " + e.getMessage(),
          e);
    }
  }


  protected boolean handleErrors(final BaseJiraContext context,
      final BulkOperationResult<BasicIssue> result) {
    boolean hasFailures = false;
    final Iterable<BulkOperationErrorResult> bulkErrors = result.getErrors();
    final String format = "    Project %10s --> %12s: %s";
    for (final BulkOperationErrorResult error : bulkErrors) {
      final ErrorCollection errors = error.getElementErrors();
      final Map<String, String> errorMap = errors.getErrors();
      final int index = error.getFailedElementNumber();
      final List<String> jiraProjectKeys = getJiraProjectKeys();
      final String projectKeyWithError = jiraProjectKeys.get(index);
      if (!errorMap.isEmpty()) {
        if (!hasFailures) {
          context.logError("  Processing errors ...");
        }
        hasFailures = true;
        for (final Map.Entry<String, String> elementError : errorMap
            .entrySet()) {
          context.logError(String.format(format, projectKeyWithError,
              elementError.getKey(), elementError.getValue()));
        }
      }
      final Collection<String> messages = errors.getErrorMessages();
      if (!messages.isEmpty()) {
        if (!hasFailures) {
          context.logError("  Processing errors ...");
        }
        hasFailures = true;
        final String messageFormat = "    Project %10s --> %s";
        for (final String message : messages) {
          context.logError(
              String.format(messageFormat, projectKeyWithError, message));
        }
      }
    }
    return hasFailures;
  }


  private void linkIssue(final IssueRestClient issueClient,
      final Issue jiraRootIssue, final BasicIssue issue)
      throws InterruptedException, ExecutionException {
    final LinkIssuesInput linkIssuesInput;
    final String issueLinkComment = getIssueLinkComment();
    final String issueLinkType = getIssueLinkType();
    if (StringUtils.isNotBlank(issueLinkComment)) {
      final Comment comment = Comment.valueOf(issueLinkComment);
      linkIssuesInput = new LinkIssuesInput(jiraRootIssue.getKey(),
          issue.getKey(), issueLinkType, comment);
    } else {
      linkIssuesInput = new LinkIssuesInput(jiraRootIssue.getKey(),
          issue.getKey(), issueLinkType);
    }
    final Promise<Void> linkPromise = issueClient.linkIssue(linkIssuesInput);
    linkPromise.get();
  }

  private void resolveIssue(final IssueRestClient issueClient,
      final Issue jiraRootIssue, final BasicIssue issue)
      throws InterruptedException, ExecutionException {
    final String issueTransition = getIssueTransition();
    if (StringUtils.isBlank(issueTransition)) {
      return;
    }
    final Promise<Issue> issuePromise = issueClient.getIssue(issue.getKey());
    final Issue fullIssue = issuePromise.get();

    final Promise<Iterable<Transition>> transitionPromise =
        issueClient.getTransitions(fullIssue.getTransitionsUri());
    final Iterable<Transition> transitions = transitionPromise.get();
    for (final Transition transition : transitions) {
      final String name = transition.getName();
      if (issueTransition.equals(name)) {
        final FieldInput resolution = calcResolution(jiraRootIssue);

        final TransitionInput transitionInput;
        if (resolution != null) {
          transitionInput = new TransitionInput(transition.getId(),
              Collections.singletonList(resolution));
        } else {
          transitionInput = new TransitionInput(transition.getId());
        }
        final Promise<Void> promise = issueClient
            .transition(fullIssue.getTransitionsUri(), transitionInput);
        promise.get();
      }
    }
  }

  private FieldInput calcResolution(final Issue jiraRootIssue) {
    final FieldInput resolution;
    final String issueResolution = getIssueResolution();
    if (SMARTICS_JIRA_SKIP_VALUE.equals(issueResolution)) {
      resolution = null;
    } else {
      if (StringUtils.isNotBlank(issueResolution)) {
        resolution = new FieldInput(FIELD_KEY_RESOLUTION,
            ComplexIssueInputFieldValue.with("name", issueResolution));
      } else {
        final Resolution rootIssueResolution = jiraRootIssue.getResolution();
        if (rootIssueResolution != null) {
          resolution =
              new FieldInput(FIELD_KEY_RESOLUTION, ComplexIssueInputFieldValue
                  .with("name", rootIssueResolution.getName()));
        } else {
          resolution = null;
        }
      }
    }
    return resolution;
  }

  protected void init(final BaseJiraContext context) {
    if (StringUtils.isBlank(getRootIssue())) {
      throw new IllegalArgumentException(
          "The parameter 'rootIssue' must be specified, but is not.");
    }
    provideJiraMetadata(getJiraProjectKeys());
    super.init();

    final File issueDescriptorFile = getIssueDescriptorFile();
    if (issueDescriptorFile != null) {
      if (issueDescriptorFile.canRead()) {
        context.logVerbose("Using issue descriptor file: "
            + issueDescriptorFile.getAbsolutePath());
        final IssueDescriptor issueDescriptor =
            IssueDescriptionParser.parse(issueDescriptorFile);
        issueSummary = StringUtilities
            .normalizeText(issueDescriptor.getSummary(getIssueSummary()));
        issueDescription = StringUtilities.normalizeText(
            issueDescriptor.getDescription(getIssueDescription()));
        issueType = StringUtilities
            .normalizeText(issueDescriptor.getType(getIssueType()));
        issuePriority = StringUtilities
            .normalizeText(issueDescriptor.getPriority(getIssuePriority()));
        issueAssignee = StringUtilities
            .normalizeText(issueDescriptor.getAssignee(getIssueAssignee()));
        issueComponents = StringUtilities
            .normalizeText(issueDescriptor.getComponents(getIssueComponents()));
        issueTransition = StringUtilities
            .normalizeText(issueDescriptor.getTransition(getIssueTransition()));
        issueResolution = StringUtilities
            .normalizeText(issueDescriptor.getResolution(getIssueResolution()));
        issueLinkComment = StringUtilities.normalizeText(
            issueDescriptor.getLinkComment(getIssueLinkComment()));
        issueLinkType = StringUtilities
            .normalizeText(issueDescriptor.getLinkType(getIssueLinkType()));
      } else {
        throw new IllegalArgumentException("Cannot read issue descriptor file: "
            + issueDescriptorFile.getAbsolutePath());
      }
    }

    if (issueDescriptorFile == null
        && (StringUtils.isBlank(this.issueDescription)
            && StringUtils.isBlank(getIssueDescription()))) {
      final String message =
          "=> ALERT: No descriptor file is specified and no description is"
              + " provided either. Please make sure to provide a reference"
              + " to a descriptor file or specify all parameters on the"
              + " command line, including the description."
              + " See https://www.smartics.eu/confluence/x/AIG0Bg for details.";
      if (context.isDryRun()) {
        context.logWarn(message);
      } else {
        if (!isIssueDescriptionIsOptional()) {
          throw new IllegalArgumentException(message);
        }
      }
    }

    final String issueLinkType = getIssueLinkType();
    if (issueLinkType == null) {
      context.logVerbose("Using default value 'Blocks' for link type.");
      this.issueLinkType = "Blocks";
    }
    final String issueResolution = getIssueResolution();
    if (issueResolution == null) {
      context.logVerbose("Using default value '" + SMARTICS_JIRA_SKIP_VALUE
          + "' for resolution.");
      this.issueResolution = SMARTICS_JIRA_SKIP_VALUE;
    }
  }

  private IssueInput createIssue(final BaseJiraContext context,
      final JiraRestClient client, final String jiraProjectKey,
      final Issue jiraRootIssue) {
    final IssueInputBuilder builder =
        setVersions(context, client, jiraProjectKey, jiraRootIssue);
    if (builder == null) {
      return null;
    }

    final String issueAssignee = getIssueAssignee();
    if (StringUtils.isNotBlank(issueAssignee)) {
      builder.setAssigneeName(issueAssignee);
    } else {
      builder.setAssignee(jiraRootIssue.getAssignee());
    }

    final String issueComponents = getIssueComponents();
    final List<String> componentNames =
        StringUtilities.splitByCommaToList(issueComponents);
    if (!componentNames.isEmpty()) {
      builder.setComponentsNames(componentNames);
    }
    final String issueDescription = getIssueDescription();
    if (StringUtils.isNotBlank(issueDescription)) {
      builder.setDescription(issueDescription);
    }

    return builder.build();
  }

  private IssueInputBuilder setVersions(final BaseJiraContext context,
      final JiraRestClient client, final String jiraProjectKey,
      final Issue jiraRootIssue) {
    final ProjectRestClient projectClient = client.getProjectClient();
    final Promise<Project> projectPromise =
        projectClient.getProject(jiraProjectKey);
    try {
      final Project project = projectPromise.get();

      final IssueType issueType = selectIssueType(project, jiraRootIssue);
      final String issueSummary = getIssueSummary();
      final IssueInputBuilder builder = new IssueInputBuilder(project,
          issueType, StringUtils.isNotBlank(issueSummary) ? issueSummary
              : jiraRootIssue.getSummary());

      final BasicPriority priority =
          selectPriority(context, client, jiraRootIssue);
      builder.setPriority(priority);

      final Iterable<Version> versions = project.getVersions();
      final PreviousVersionSelector selector =
          new PreviousVersionSelector(versions);
      final Version nextReleaseVersion = selector.selectNextRelease();
      if (nextReleaseVersion != null) {
        builder.setFixVersions(Collections.singletonList(nextReleaseVersion));
      }
      final Version lastReleaseVersion = selector.selectLastRelease();
      if (lastReleaseVersion != null) {
        builder
            .setAffectedVersions(Collections.singletonList(lastReleaseVersion));
      }
      return builder;
    } catch (final InterruptedException e) {
      final String rootIssue = getRootIssue();
      context.logError(
          "Skipping project '" + jiraProjectKey + "' for issue '" + rootIssue
              + "' because timed out talking to JIRA: " + e.getMessage());
    } catch (final ExecutionException e) {
      logRestCommunicationProblems(context, jiraProjectKey, e);
    }
    return null;
  }

  private IssueType selectIssueType(final Project project,
      final Issue jiraRootIssue) {
    final String issueType = getIssueType();
    if (StringUtils.isNotBlank(issueType)) {
      for (final IssueType issueTypeInstance : project.getIssueTypes()) {
        final String name = issueTypeInstance.getName();
        if (issueType.equals(name)) {
          return issueTypeInstance;
        }
      }

      final StringBuilder buffer = new StringBuilder(64);
      for (final IssueType type : project.getIssueTypes()) {
        buffer.append(type.getName()).append(", ");
      }
      if (buffer.length() > 0) {
        buffer.setLength(buffer.length() - 2);
      }
      throw new IllegalArgumentException("Unknown issue type '" + issueType
          + "'. Known values are: " + buffer);
    } else {
      return jiraRootIssue.getIssueType();
    }
  }

  private BasicPriority selectPriority(final BaseJiraContext context,
      final JiraRestClient client, final Issue jiraRootIssue) {
    final String issuePriority = getIssuePriority();
    if (StringUtils.isNotBlank(issuePriority)) {
      final List<Priority> priorities = getPriorities(context, client);
      for (final Priority currentPriority : priorities) {
        final String name = currentPriority.getName();
        if (issuePriority.equals(name)) {
          return currentPriority;
        }
      }

      final StringBuilder buffer = new StringBuilder(64);
      for (final Priority currentPriority : priorities) {
        buffer.append(currentPriority.getName()).append(", ");
      }
      if (buffer.length() > 0) {
        buffer.setLength(buffer.length() - 2);
      }
      throw new IllegalArgumentException("Unknown issue priority '"
          + issuePriority + "'. Known values are: " + buffer);
    } else {
      return jiraRootIssue.getPriority();
    }
  }


  private List<Priority> getPriorities(final BaseJiraContext context,
      final JiraRestClient client) {
    final MetadataRestClient metadataClient = client.getMetadataClient();
    final List<Priority> priorityList = new ArrayList<>();
    try {
      final Promise<Iterable<Priority>> priorityPromise =
          metadataClient.getPriorities();
      final Iterable<Priority> priorities = priorityPromise.get();
      for (final Priority currentPriority : priorities) {
        priorityList.add(currentPriority);
      }
    } catch (final InterruptedException e) {
      final String rootIssue = getRootIssue();
      context.logError("Cannot fetch priority '" + issuePriority
          + "' for issue '" + rootIssue
          + "' because timed out talking to JIRA: " + e.getMessage());
    } catch (final ExecutionException e) {
      final String rootIssue = getRootIssue();
      context.logError("Cannot fetch priority '" + issuePriority
          + "' for issue '" + rootIssue
          + "' because communication with JIRA failed: " + e.getMessage());
    }
    return priorityList;
  }

  // --- object basics --------------------------------------------------------

}
