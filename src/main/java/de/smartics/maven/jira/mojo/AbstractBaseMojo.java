/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.mojo;

import de.smartics.maven.jira.domain.AppException;
import de.smartics.maven.jira.domain.ExecutionContext;
import de.smartics.maven.jira.tools.maven.UserPropertiesManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * Base implementation for all mojos in this project.
 *
 * @since 1.0
 */
public abstract class AbstractBaseMojo extends AbstractMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * Generic value to tell to skip the generation of a value. This is used to
   * prevent to calculate the default value from the root issue or any other
   * resource.
   */
  protected static final String SMARTICS_JIRA_SKIP_VALUE = "smartics-jira.skip";

  // --- members --------------------------------------------------------------

  /**
   * Project for the execution environment.
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "${project}", readonly = true)
  protected MavenProject mavenProject;

  /**
   * Session for the execution environment.
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "${session}", readonly = true)
  protected MavenSession mavenSession;

  /**
   * A simple flag to skip the deployment process.
   * <p>
   * Only support to be set with <tt>-Dsmartics-jira.skip</tt>.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.skip", defaultValue = "false")
  private boolean skip;

  /**
   * A simple flag to log verbosely.
   * <p>
   * Set with '<tt>-Dsmartics-jira.verbose</tt>' or '<tt>-Dverbose</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.verbose", defaultValue = "false")
  private boolean verbose;

  /**
   * A simple flag to populate the source, but without actually deploying to the
   * target.
   * <p>
   * Set with '<tt>-Dsmartics-jira.dryRun</tt>' or '<tt>-DdryRun</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.dryRun", defaultValue = "false")
  private boolean dryRun;

  /**
   * Provides access to additional properties set on the commandline.
   */
  private UserPropertiesManager userPropertiesManager;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  public class BaseContext implements ExecutionContext {

    private final Log log;

    protected BaseContext() {
      this.log = getLog();
    }

    @Override
    public void logInfo(final String message) {
      log.info(message);
    }

    @Override
    public void logDebug(final String message) {
      log.debug(message);
    }

    @Override
    public boolean isDebug() {
      return log.isDebugEnabled();
    }

    public void logWarn(final String message) {
      log.warn(message);
    }

    public void logError(final String message) {
      log.error(message);
    }

    public void logError(final String message, final Throwable e) {
      log.error(message, e);
    }

    @Override
    public void logVerbose(final String message) {
      if (isVerbose()) {
        log.info(message);
      }
    }

    @Override
    public void logVerbose(final Object appSource) {
      if (isVerbose()) {
        log.info(appSource.toString());
      }
    }

    @Override
    public boolean isVerbose() {
      return AbstractBaseMojo.this.isVerbose();
    }

    @Override
    public boolean isDryRun() {
      return AbstractBaseMojo.this.isDryRun();
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the version of the Maven project.
   *
   * @return the version or <code>null</code> if the version is not set or no
   *         project is found.
   */
  protected String getProjectVersion() {
    if (mavenProject != null) {
      final String version = mavenProject.getVersion();
      if (StringUtils.isNotBlank(version)) {
        return version;
      }
    }

    return null;
  }

  public UserPropertiesManager getUserPropertiesManager() {
    if (userPropertiesManager == null) {
      userPropertiesManager = new UserPropertiesManager(mavenSession);
    }
    return userPropertiesManager;
  }

  protected boolean isSkip() {
    return skip;
  }

  private boolean isVerbose() {
    return verbose
        || getUserPropertiesManager().getPropertyAsBoolean("verbose");
  }

  private boolean isDryRun() {
    return dryRun || getUserPropertiesManager().getPropertyAsBoolean("dryRun");
  }

  protected boolean hasPom() {
    return mavenProject != null && !mavenProject.getId()
        .startsWith("org.apache.maven:standalone-pom:pom:");
  }

  // --- business -------------------------------------------------------------

  protected void init() throws AppException {}

  // --- object basics --------------------------------------------------------

}
