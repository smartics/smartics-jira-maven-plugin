/*
 * Copyright 2019-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.jira.mojo;


import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.RestClientException;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.google.common.base.Optional;
import de.smartics.maven.jira.domain.AppException;
import de.smartics.maven.jira.tools.security.SettingsDecrypter;
import org.apache.maven.model.IssueManagement;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.codehaus.plexus.util.StringUtils;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcher;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcherException;

import java.net.URI;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Base implementation for all mojos accessing the JIRA server in this project.
 *
 * @since 1.0
 */
public abstract class AbstractJiraMojo extends AbstractBaseMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Helper to decrypt passwords from the settings.
   *
   * @since 1.0
   */
  @Component(
      role = org.sonatype.plexus.components.sec.dispatcher.SecDispatcher.class)
  private SecDispatcher securityDispatcher;

  /**
   * The location of the <code>settings-security.xml</code>.
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "${user.home}/.m2/settings-security.xml")
  private String settingsSecurityLocation;

  /**
   * The name of the user to authenticate to the remote server.
   * <p>
   * Set with '<tt>-Dsmartics-jira.username</tt>', '<tt>-Djira.username</tt>',
   * or '<tt>-Dusername</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.username")
  private String username;

  /**
   * The password of the user to authenticate to the remote server.
   * <p>
   * Set with '<tt>-Dsmartics-jira.password</tt>', '<tt>-Djira.password</tt>',
   * or '<tt>-Dpassword</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.password")
  private String password;

  /**
   * The identifier for the server to fetch credentials in case
   * <tt>username</tt> or <tt>password</tt> are not set explicitly. The
   * credentials are used to connect to the REST API of the remote server.
   * <p>
   * Defaults to '<tt>jira</tt>'.
   * </p>
   * <p>
   * Set with '<tt>-Dsmartics-jira.serverId</tt>', '<tt>-Djira.serverId</tt>',
   * or '<tt>-DserverId</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.serverId")
  private String serverId;

  /**
   * The base server URL to locate services. This value is required, and is
   * probably set via a profile in case more than one environment is targeted.
   * Per convention, profiles containing a environment specific configuration,
   * do not contain lower case letters.
   * <p>
   * Set with '<tt>-Dsmartics-jira.serverUrl</tt>', '<tt>-Djira.serverUrl</tt>',
   * or '<tt>-DserverUrl</tt>'.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "smartics-jira.serverUrl")
  private String serverUrl;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  public class BaseJiraContext extends BaseContext {
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getUsername() {
    if (StringUtils.isBlank(username)) {
      final String jiraValue =
          getUserPropertiesManager().getProperty("jira", "username");
      if (StringUtils.isNotBlank(jiraValue)) {
        return jiraValue;
      }
      return getUserPropertiesManager().getProperty("username");
    }
    return username;
  }

  public String getPassword() {
    if (StringUtils.isBlank(password)) {
      final String jiraValue =
          getUserPropertiesManager().getProperty("jira", "password");
      if (StringUtils.isNotBlank(jiraValue)) {
        return jiraValue;
      }
      return getUserPropertiesManager().getProperty("password");
    }
    return password;
  }

  public String getServerId() {
    if (StringUtils.isBlank(serverId)) {
      final String jiraValue =
          getUserPropertiesManager().getProperty("jira", "serverId");
      if (StringUtils.isNotBlank(jiraValue)) {
        return jiraValue;
      }
      final String globalValue =
          getUserPropertiesManager().getProperty("serverId");
      if (StringUtils.isNotBlank(globalValue)) {
        return globalValue;
      }
      serverId = "jira";
    }
    return serverId;
  }

  public String getServerUrl() {
    if (StringUtils.isBlank(serverUrl)) {
      return getUserPropertiesManager().getProperty("serverUrl");
    }
    return serverUrl;
  }

  // --- business -------------------------------------------------------------

  protected void init() throws AppException {
    final boolean hasUsername = StringUtils.isNotBlank(getUsername());
    final boolean hasPassword = StringUtils.isNotBlank(getPassword());
    if (!(hasUsername && hasPassword)) {
      provideCredentialsFromServerConfig(hasUsername, hasPassword);
    }

    if (StringUtils.isBlank(getServerUrl())) {
      throw new IllegalArgumentException(
          "URL to JIRA server must be set, but is missing."
              + " Use -Dsmartics-jira.serverUrl to specifiy the URL the JIRA server.");
    }
  }

  protected JiraRestClient createJiraClient(final BaseJiraContext context) {
    final String serverUrl = getServerUrl();
    final String username = getUsername();
    final String password = getPassword();
    context.logVerbose(
        "Authenticate to '" + serverUrl + "' as user '" + username + "' using "
            + (StringUtils.isNotBlank(password) ? "" : "no ") + "password.");
    return new AsynchronousJiraRestClientFactory()
        .createWithBasicHttpAuthentication(URI.create(serverUrl), username,
            password);
  }

  private void provideCredentialsFromServerConfig(final boolean hasUsername,
      final boolean hasPassword) throws AppException {
    final Server server = fetchServer();
    if (!hasUsername) {
      username = server.getUsername();
    }
    if (!hasPassword) {
      final String plain = server.getPassword();
      try {
        final SettingsDecrypter settingsDecrypter =
            new SettingsDecrypter(securityDispatcher, settingsSecurityLocation);
        password = settingsDecrypter.decrypt(plain);
      } catch (final SecDispatcherException e) {
        final String serverId = getServerId();
        getLog().warn("Cannot decrypt password for server '" + serverId + "': "
            + e.getMessage()
            + " -> Using plain password which will work if the password"
            + " is actually not encrypted. See"
            + " https://maven.apache.org/guides/mini/guide-encryption.html"
            + " for details.");
        password = plain;
      }
    }
  }

  private Server fetchServer() throws AppException {
    final Settings settings = mavenSession.getSettings();
    final String serverId = getServerId();
    if (settings == null) {
      throw new AppException(
          "No settings provided to find server configuration '" + serverId
              + "'. Cannot provide credentials. See"
              + " https://maven.apache.org/settings.html for details.");
    }
    final Server server = settings.getServer(serverId);
    if (server == null) {
      throw new AppException("No server configuration found for '" + serverId
          + "'. Cannot provide credentials. See"
          + " https://maven.apache.org/settings.html#Servers for details.");
    }
    return server;
  }

  protected void logRestCommunicationProblems(final BaseJiraContext context,
      final String jiraProjectKey, final ExecutionException e) {
    final String message = "Skipping '" + jiraProjectKey
        + "' because of communication problems talking to JIRA: ";
    logRestCommunicationProblems(context, jiraProjectKey, message, e);
  }

  protected void logRestCommunicationProblems(final BaseJiraContext context,
      final String jiraProjectKey, final String message,
      final ExecutionException e) {
    final Throwable cause = e.getCause();
    if (cause instanceof RestClientException) {
      final RestClientException restCause = (RestClientException) cause;
      final Optional<Integer> statusCodeOptional = restCause.getStatusCode();
      if(statusCodeOptional.isPresent()) {
        final int statusCode = statusCodeOptional.get();
        final String serverUrl = getServerUrl();
        if (statusCode == 401 || statusCode == 403) {
          context.logError("HTTP " + statusCode + " from " + serverUrl +
                           "\n Cannot access resources on JIRA due to " +
                           (statusCode == 401 ? "authentication" :
                            "authorization") + " problems.");
        } else {
          context.logError(
              "HTTP " + statusCode + " from " + serverUrl + "\n" + message +
              restCause.getMessage());
        }
      } else {
        context.logError(
            "HTTP request from " + serverUrl + " failed!\n" + message +
            restCause.getMessage());
      }
    } else {
      context.logError("Skipping '" + jiraProjectKey
          + "' because of communication problems talking to JIRA: "
          + e.getMessage());
    }
  }

  protected void provideJiraMetadata(final List<String> jiraProjectKeys) {
    final boolean hasProjectKeys = !jiraProjectKeys.isEmpty();
    if (hasPom()) {
      final String serverUrl = getServerUrl();
      final boolean hasServerUrl = StringUtils.isNotBlank(serverUrl);
      if (!(hasProjectKeys && hasServerUrl)) {
        final IssueManagement issueManagement =
            mavenProject.getIssueManagement();
        if (issueManagement != null) {
          final String url = issueManagement.getUrl();
          if (StringUtils.isNotBlank(url)) {
            if (!hasProjectKeys) {
              final int index = url.lastIndexOf('/');
              if (index > -1 && index < url.length() - 1) {
                final String projectKey = url.substring(index + 1);
                jiraProjectKeys.add(projectKey);
              }
            }
            if (!hasServerUrl) {
              final int index = url.lastIndexOf("/projects/");
              if (index > -1) {
                this.serverUrl = url.substring(0, index);
              }

              final String system = issueManagement.getSystem();
              if (StringUtils.isNotBlank(system)
                  && StringUtils.isBlank(this.serverId)) {
                this.serverId = system;
              }
            }
            return;
          }
        }
      }
    }
    if (!hasProjectKeys) {
      throw new IllegalArgumentException(
          "JIRA project keys must not be empty. Please specify the key for"
              + " those projects to process with -Dsmartics-jira.projectKeys!");
    }
  }

  // --- object basics --------------------------------------------------------

}
